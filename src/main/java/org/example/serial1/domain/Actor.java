package org.example.serial1.domain;

import java.time.LocalDate;

public class Actor extends Entity implements DomainObject {

    private int id;

    private String name;
    LocalDate dateOfBirth;
    private String biography;

    public Actor(String name, LocalDate dateOfBirth, String biography) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.biography = biography;
    }

    public Actor() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }
}
