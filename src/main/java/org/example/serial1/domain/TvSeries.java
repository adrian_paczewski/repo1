package org.example.serial1.domain;

public class TvSeries {

    private long id;

    private String name;
    private int seasons;

    public TvSeries() {
    }

    public TvSeries(String name, int seasons) {
        this.name = name;
        this.seasons = seasons;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSeasons(int seasons) {
        this.seasons = seasons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeasons() {
        return seasons;
    }

    @Override
    public String toString() {
        return "TvSeries{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", seasons=" + seasons +
                '}';
    }
}

