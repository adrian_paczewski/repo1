package org.example.serial1.domain;

public enum EntityState {
    New, Changed, Unchanged, Deleted
}
