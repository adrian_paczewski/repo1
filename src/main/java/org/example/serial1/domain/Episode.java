package org.example.serial1.domain;

import java.time.Duration;
import java.time.LocalDate;

public class Episode {

    private long id;

    private String name;
    LocalDate releaseDate;
    private int episodeNumber;
    Duration duration;

    public Episode() {
    }

    public Episode(String name, LocalDate releaseDate, int episodeNumber, Duration duration) {

        this.name = name;
        this.releaseDate = releaseDate;
        this.episodeNumber = episodeNumber;
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}
