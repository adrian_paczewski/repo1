package org.example.serial1.db;

import org.example.serial1.db.mapper.AbstractMapper;
import org.example.serial1.domain.Entity;
import org.example.serial1.unitofwork.IUnitOfWork;
import org.example.serial1.unitofwork.IUnitOfWorkRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class Repository <T extends Entity>
        implements IRepository<T>, IUnitOfWorkRepository {


    protected IUnitOfWork uow;
    protected Connection connection;
    protected PreparedStatement selectByID;
    protected PreparedStatement insert;
    protected PreparedStatement delete;
    protected PreparedStatement update;
    protected PreparedStatement selectAll;
    protected IEntityBuilder<T> builder;

    protected String selectByIDSql=
            "SELECT * FROM "
                    + getTableName()
                    + " WHERE id=?";
    protected String deleteSql=
            "DELETE FROM "
                    + getTableName()
                    + " WHERE id=?";
    protected String selectAllSql=
            "SELECT * FROM " + getTableName();


    protected Repository(Connection connection,
                         IEntityBuilder<T> builder, IUnitOfWork uow)
    {
        this.uow=uow;
        this.builder=builder;
        this.connection = connection;
        try {
            selectByID=connection.prepareStatement(selectByIDSql);
            insert = connection.prepareStatement(getInsertQuery());
            delete = connection.prepareStatement(deleteSql);
            update = connection.prepareStatement(getUpdateQuery());
            selectAll = connection.prepareStatement(selectAllSql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Repository() {
    }

    @Override
    public void add(Entity entity)
    {
        uow.markAsNew(entity, this);
    }

    @Override
    public void update(Entity entity)
    {
        uow.markAsDirty(entity, this);
    }

    @Override
    public void delete(Entity entity)
    {
        uow.markAsDeleted(entity, this);
    }

    @Override
    public void persistAdd(Entity entity) {

        try {
            setUpInsertQuery((T)entity);
            insert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void persistUpdate(Entity entity) {

        try {
            setUpUpdateQuery((T)entity);
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void persistDelete(Entity entity) {

        try {
            delete.setInt(1, entity.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public T get(int id) {

        try {
            selectByID.setInt(1, id);
            ResultSet rs = selectByID.executeQuery();
            while(rs.next())
            {
                return builder.build(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }



    @Override
    public List<T> getAll() {

        List<T> result = new ArrayList<T>();

        try {
            ResultSet rs= selectAll.executeQuery();
            while(rs.next())
            {
                result.add(builder.build(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    protected abstract void setUpUpdateQuery(T entity) throws SQLException;
    protected abstract void setUpInsertQuery(T entity) throws SQLException;
    protected abstract String getTableName();
    protected abstract String getUpdateQuery();
    protected abstract String getInsertQuery();

}
