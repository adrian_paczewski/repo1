package org.example.serial1.db.catalogs;

import org.example.serial1.db.ActorRepository;
import org.example.serial1.db.RepositoryCatalog;
import org.example.serial1.db.repos.HsqlActorRepository;

import java.sql.Connection;

public class HsqlRepositoryCatalog implements RepositoryCatalog {

    Connection connection;


    public HsqlRepositoryCatalog(Connection connection) {
        this.connection = connection;
    }

    @Override
    public ActorRepository actors() {
        return new HsqlActorRepository(connection);
    }
}
