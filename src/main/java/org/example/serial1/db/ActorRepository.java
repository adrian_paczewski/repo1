package org.example.serial1.db;

import org.example.serial1.domain.Actor;
import org.example.serial1.domain.Entity;
import org.example.serial1.unitofwork.IUnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

public class ActorRepository extends Repository<Actor> {

    public ActorRepository(Connection connection, IEntityBuilder<Actor> builder,
                              IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    public void add(Entity entity) {
        super.add(entity);
    }

    @Override
    public void update(Entity entity) {
        super.update(entity);
    }

    @Override
    public void delete(Entity entity) {
        super.delete(entity);
    }

    @Override
    public void persistAdd(Entity entity) {
        super.persistAdd(entity);
    }

    @Override
    public void persistUpdate(Entity entity) {
        super.persistUpdate(entity);
    }

    @Override
    public void persistDelete(Entity entity) {
        super.persistDelete(entity);
    }

    public ActorRepository() {
    }

    @Override
    protected void setUpUpdateQuery(Actor entity) throws SQLException {
        update.setInt(1, entity.getId());
        update.setString(2, entity.getName());
        update.setString(3, entity.getBiography());
    }

    @Override
    protected void setUpInsertQuery(Actor entity) throws SQLException {
        insert.setString(2, entity.getName());
        insert.setString(3, entity.getBiography());

    }

    @Override
    public String getTableName() {
        return "actor";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE actor SET (name, biography) = (?, ?) WHERE id = ?";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO actor (name, biography) VALUES(?, ?)";
    }


}
