package org.example.serial1.db;

import java.util.List;

public interface IRepository<T> {

    void add(T entity);
    void update(T entity);
    void delete(T entity);
    T get(int id);
    List<T> getAll();
}
