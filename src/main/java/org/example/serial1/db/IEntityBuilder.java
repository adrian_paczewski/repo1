package org.example.serial1.db;

import org.example.serial1.domain.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IEntityBuilder <T extends Entity> {

    T build(ResultSet rs) throws SQLException;

}
