package org.example.serial1.db.repos;

import org.example.serial1.db.mapper.ActorMapper;
import org.example.serial1.domain.Actor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class HsqlActorRepository {

    private Connection connection;

    private ActorMapper actorMapper;

    private String createActorTable = ""
            + "CREATE TABLE Actor("
            + "id bigint GENERATED BY DEFAULT AS IDENTITY,"
            + "name VARCHAR(20),"
            + "dateOfBirth TIME,"
            + "biography TEXT"
            + ")";

    public HsqlActorRepository(Connection connection) {
        this.connection = connection;

        this.actorMapper = new ActorMapper(connection);

        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("Actor")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(createActorTable);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }



    public void add(Actor actor) {
        actorMapper.add(actor);

    }

    public void modify(Actor actor) {
        actorMapper.update(actor);
    }

    public void remove(Actor actor) {
        actorMapper.remove((long) actor.getId());
    }



}
