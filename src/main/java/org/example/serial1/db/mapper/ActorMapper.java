package org.example.serial1.db.mapper;

import org.example.serial1.domain.Actor;

import java.sql.*;

public class ActorMapper extends AbstractMapper<Actor> {

    private static final String COLUMNS = "id, name, dateOfBirth, biography";
    public static final String INSERT_STMT = "INSERT INTO actor(name, dateOfBirth, biography) VALUES (?, ?, ?)";
    public static final String UPDATE_STMT = "UPDATE actor SET (name, dateOfBirth, biography)=(?, ?) WHERE id = ?";
    public static final String DELETE_STMT = "DELETE FROM actor WHERE id =?";

    public ActorMapper(Connection connection){
        super(connection);

    }

    protected Actor doLoad(ResultSet rs) throws SQLException {
        Actor actor = new Actor();
        actor.setId(rs.getInt("id"));
        actor.setName(rs.getString("name"));
        actor.setBiography(rs.getString("biography"));
        actor.setDateOfBirth(rs.getDate("dateOfBirth").toLocalDate());
        return actor;
    }

    @Override
    protected void parametrizeInsertStatement(PreparedStatement statement, Actor actor) throws SQLException {

        statement.setString(1, actor.getName());
        statement.setDate(2, Date.valueOf(actor.getDateOfBirth()));
        statement.setString(3, actor.getBiography());

    }

    @Override
    protected void parametrizeUpdateStatement(PreparedStatement statement, Actor actor) throws SQLException {

        parametrizeInsertStatement(statement, actor);
        statement.setLong(4, actor.getId());

    }

    @Override
    protected String insertStatement() {
        return INSERT_STMT;
    }

    @Override
    protected String updateStatement() {
        return UPDATE_STMT;
    }

    @Override
    protected String removeStatement() {
        return DELETE_STMT;
    }


}
