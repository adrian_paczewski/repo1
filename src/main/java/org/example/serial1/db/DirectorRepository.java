package org.example.serial1.db;

import org.example.serial1.domain.Director;
import org.example.serial1.unitofwork.IUnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

public class DirectorRepository extends Repository<Director> {

    protected DirectorRepository(Connection connection, IEntityBuilder<Director> builder,
                              IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    protected void setUpUpdateQuery(Director entity) throws SQLException {
        update.setInt(1, entity.getId());
        update.setString(2, entity.getName());
        update.setString(3, entity.getBiography());

    }

    @Override
    protected void setUpInsertQuery(Director entity) throws SQLException {
        insert.setString(2, entity.getName());
        insert.setString(3, entity.getBiography());

    }

    @Override
    protected String getTableName() {
        return "director";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE director SET (name, biography) = (?, ?) WHERE id = ?";
    }

    @Override
    protected String getInsertQuery() {
        return "INSERT INTO director (name, biography) VALUES(?, ?)";

    }
}
