package org.example.serial1.db;

import org.example.serial1.domain.Actor;
import org.example.serial1.domain.Director;

public interface IRepositoryCatalog {

    public IRepository<Actor> getActors();
    public IRepository<Director> getDirectors();
    public void commit();
}