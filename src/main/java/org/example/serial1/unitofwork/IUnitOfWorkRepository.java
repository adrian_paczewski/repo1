package org.example.serial1.unitofwork;

import org.example.serial1.domain.Entity;

public interface IUnitOfWorkRepository {

    void persistAdd(Entity entity);
    void persistUpdate(Entity entity);
    void persistDelete(Entity entity);
}
