package org.example.serial1.unitofwork;

import org.example.serial1.domain.Entity;

public interface IUnitOfWork {

    void commit();
    void rollback();
    void markAsNew(Entity entity, IUnitOfWorkRepository repository);
    void markAsDirty(Entity entity, IUnitOfWorkRepository repository);
    void markAsDeleted(Entity entity, IUnitOfWorkRepository repository);

}
