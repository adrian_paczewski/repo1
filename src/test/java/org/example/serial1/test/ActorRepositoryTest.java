package org.example.serial1.test;

import org.example.serial1.db.ActorRepository;
import org.example.serial1.db.IEntityBuilder;
import org.example.serial1.domain.Actor;
import org.example.serial1.unitofwork.IUnitOfWork;
import org.junit.Test;

import java.sql.Connection;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class ActorRepositoryTest {

    @Test
    public void itCorrectlyReturnTableName() throws Exception {
        ActorRepository actorRepository = new ActorRepository();
        assertEquals("actor", actorRepository.getTableName());
    }

    @Test
    public void itCorrectlyReturnUpdateQuery() throws Exception {
        ActorRepository actorRepository = new ActorRepository();
        assertEquals("UPDATE actor SET (name, biography) = (?, ?) WHERE id = ?", actorRepository.getUpdateQuery());

    }

    @Test
    public void itCorrectlyReturnInsertQuery() throws Exception {
        ActorRepository actorRepository = new ActorRepository();
        assertEquals("INSERT INTO actor (name, biography) VALUES(?, ?)", actorRepository.getInsertQuery());
    }



    @Test
    public void checkAdding() throws Exception {
         IUnitOfWork uow;
         Connection connection;
         IEntityBuilder<Actor> builder;
        Actor actor = new Actor("name", LocalDate.of(1970, 1, 1), "biography");
        ActorRepository actorRepository = new ActorRepository(connection, builder, uow);

        assertEquals(1, actorRepository.add(actor));

    }

    }
