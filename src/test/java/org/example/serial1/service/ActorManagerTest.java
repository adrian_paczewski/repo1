package org.example.serial1.service;

import org.example.serial1.domain.Actor;
import org.junit.Test;
import service.ActorManager;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ActorManagerTest {

    ActorManager actorManager = new ActorManager();

    private final static String NAME_1 = "Actor 1";
    private final static LocalDate DOB_1 = LocalDate.of(1970, 1, 1);
    private final static String BIOGRAPHY = "Actor biography";

    @Test
    public void checkConnection() {
        assertNotNull(actorManager.getConnection());
    }

    @Test
    public void checkAdding() {
        Actor actor = new Actor(NAME_1, DOB_1, BIOGRAPHY);

        actorManager.clearActor();
        assertEquals(1, actorManager.addActor(actor));

        List<Actor> actorList = actorManager.getAllActors();
        Actor actorRetrieved = actorList.get(0);

        assertEquals(NAME_1, actorRetrieved.getName());
        assertEquals(DOB_1, actorRetrieved.getDateOfBirth());
        assertEquals(BIOGRAPHY, actorRetrieved.getBiography());

    }

}
