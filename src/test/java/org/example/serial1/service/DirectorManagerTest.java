package org.example.serial1.service;

import org.example.serial1.domain.Director;
import org.junit.Test;
import service.DirectorManager;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DirectorManagerTest {

    DirectorManager directorManager = new DirectorManager();

    private final static String NAME_1 = "Director 1";
    private final static LocalDate DOB_1 = LocalDate.of(1970, 1, 1);
    private final static String BIOGRAPHY = "Director biography";

    @Test
    public void checkConnection() {
        assertNotNull(directorManager.getConnection());
    }

    @Test
    public void checkAdding() {
        Director director = new Director(NAME_1, DOB_1, BIOGRAPHY);

        directorManager.clearDirector();
        assertEquals(1, directorManager.addDirector(director));

        List<Director> directorList = directorManager.getAllDirectors();
        Director directorRetrieved = directorList.get(0);

        assertEquals(NAME_1, directorRetrieved.getName());
        assertEquals(DOB_1, directorRetrieved.getDateOfBirth());
        assertEquals(BIOGRAPHY, directorRetrieved.getBiography());

    }
}
