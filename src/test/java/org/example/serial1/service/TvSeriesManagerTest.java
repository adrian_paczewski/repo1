package org.example.serial1.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import org.example.serial1.domain.TvSeries;
import service.TvSeriesManager;

public class TvSeriesManagerTest {

    TvSeriesManager tvSeriesManager = new TvSeriesManager();

    private final static String NAME_1 = "Serial 1";
    private final static int SEASON_1 = 9;

    @Test
    public void checkConnection() {
        assertNotNull(tvSeriesManager.getConnection());
    }

    @Test
    public void checkAdding() {

        TvSeries tvseries = new TvSeries(NAME_1, SEASON_1);

        tvSeriesManager.clearTvSeries();
        assertEquals(1, tvSeriesManager.addTvSeries(tvseries));

        List<TvSeries> tvSeries = tvSeriesManager.getAllTvSeries();
        TvSeries tvSeriesRetrieved = tvSeries.get(0);

        assertEquals(NAME_1, tvSeriesRetrieved.getName());
        assertEquals(SEASON_1, tvSeriesRetrieved.getSeasons());

    }

}
