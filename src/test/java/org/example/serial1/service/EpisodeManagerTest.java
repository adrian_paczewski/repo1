package org.example.serial1.service;

import org.example.serial1.domain.Episode;
import org.junit.Test;
import service.EpisodeManager;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EpisodeManagerTest {

    EpisodeManager episodeManager = new EpisodeManager();

    private static final String episodeName = "episode name";
    private static final LocalDate episodeReleaseDate = LocalDate.of(1970, 1, 1);
    private static final int episodeNumber = 10;
    private static final Duration episodeDuration = Duration.ofHours(10);

    @Test
    public void checkConnection() {
        assertNotNull(episodeManager.getConnection());
    }

    @Test
    public void checkAdding() {
        Episode episode = new Episode(episodeName, episodeReleaseDate, episodeNumber, episodeDuration);

        episodeManager.clearEpisode();
        assertEquals(1, episodeManager.addEpisode(episode));

        List<Episode> episodeList = episodeManager.getAllEpisodes();
        Episode episodeRetrieved = episodeList.get(0);

        assertEquals(episodeName, episodeRetrieved.getName());
        assertEquals(episodeReleaseDate, episodeRetrieved.getReleaseDate());
        assertEquals(episodeNumber, episodeRetrieved.getEpisodeNumber());
        assertEquals(episodeDuration, episodeRetrieved.getDuration());

    }
}
